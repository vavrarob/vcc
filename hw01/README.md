Nejprve jsem postupoval podle navodu ze 3. cviceni (https://gitlab.fit.cvut.cz/fesljan/ni-vcc/raw/master/cviceni3.txt).

Nasledovalo pridani dvou vypocetnich nodu upravenim sekce compute v souboru multinode.

> [compute]

> vavrarob-01.vcc

> vavrarob-02.vcc

A opetovny deploy.

> kolla-ansible -i ./multinode deploy

Pote vytvoreni dvou instanci prikazy

> openstack server create --flavor m1.tiny --image cirros --key-name mykey --availability-zone nova:vavrarob-01 --network demo-net demo1

> openstack server create --flavor m1.tiny --image cirros --key-name mykey --availability-zone nova:vavrarob-02 --network demo-net demo2

![](hypervisors.jpg)
![](network_topology.jpg)

Pingovani mezi VMs

![](ping_to_demo1.jpg)
![](ping_to_demo2.jpg)

Nastaveni pingovani do internetu pomoci prikazu

> echo 1 > /proc/sys/net/ipv4/ip_forward

> ip addr add 10.0.2.1/24 brd + dev veth1

> iptables -t nat -A POSTROUTING -s 10.0.2.0/24 -j MASQUERADE

![](ping_to_network.jpg)
