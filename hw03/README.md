## 1. cast 
Vyuzil jsem [HW03](https://gitlab.fit.cvut.cz/NI-AM2/B222/cs/vavrarob/-/tree/master/03) z AM2
![](cast_1.png)

## 2. cast
Nejprve jsem ulozil image do .tar archivu pomoci 
> docker save vavrarob/node-web-app

A nasledne vypsal jednotlive vrstvy
![](layers.jpg)
Pro analyzu vrstev jsem pouzil nastroj [dive](https://github.com/wagoodman/dive)
![](dive-analysis.jpg)
Ten mi umoznil namapovat jednotlive vrstvy na prikazy v dockerfile, napr prvni vrstva (3036ada...) vypsana pomoci prikazu 
> tar -tvf node-app.tar | grep layer.tar

opdovida prikazu RUN v dockerfile.

Jednotlive vrstvy je mozne zkoumat take pomoci prikazu 
> docker history vavrarob/node-web-app

![](history.jpg)
Prvnich pet vrstev shora odpovida mnou definovanym prikazum v dockerfile. Ostatni podkladovemu obrazu node:16.
![](dockerfile.jpg)
## 3. cast
Postup dle toho [navodu](https://wiki.archlinux.org/title/Systemd-nspawn) cast 2.3
> mkdir fedora-container

> apt-get update

> apt-get -y install dnf

> dnf --releasever=37 --best --setopt=install_weak_deps=False --repo=fedora --repo=updates --installroot=/root/fedora-container install dhcp-client dnf fedora-release glibc glibc-langpack-en iputils less ncurses passwd systemd systemd-networkd systemd-resolved util-linux vim-default-editor

> apt install systemd-container

> systemd-nspawn -D /root/fedora-container passwd

> systemd-nspawn -bUD /root/fedora-container

![](fedora-container.jpg)

## 4. cast
Opet jsem vyuzil [HW03](https://gitlab.fit.cvut.cz/NI-AM2/B222/cs/vavrarob/-/tree/master/03) z AM2

> nano /etc/systemd/system/node-app.service

![](node-app.service.jpg)

spousteni service pomoci

> systemctl daemon-reload && systemctl stop node-app.service && systemctl start node-app.servic

![](systemctl-status.jpg)

puvodni security
![](previous-security.jpg)

nova security pomoci tohoto [navodu](https://www.opensourcerers.org/2022/04/25/optimizing-a-systemd-service-for-security/)
![](final-security.jpg)

