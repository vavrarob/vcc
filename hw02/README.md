# HW02
## 1. cast

> docker exec -it nova_libvirt bash -i

> virsh list

![virsh_list](01_virsh_list.jpg)

> virsh dumpxml instance-00000004

![virsh_dumpxml](01_virsh_dumpxml.jpg)

> Poslouchani provozu mezi dvema VMs pomoci

> tcpdump -i tape2eb15b9-52

![tcpdump](tcpdump.jpg)

## 2. cast 
> tcpdump -i ens192 udp -w dump_log

![wireshark](wireshark.jpg)

## 3. cast
![network_diagram](network_diagram.jpg)

### vavrarob-01

![](01_virsh_list.jpg)
![](01_virsh_dumpxml.jpg)
![](01-show.jpg)
![](01-br-int.jpg)
![](01-br-tun.jpg)

### vavrarob-02

![](02_virsh_list.jpg)
![](02_virsh_dumpxml.jpg)
![](02-show.jpg)
![](02-br-int.jpg)
![](02-br-tun.jpg)

## 4. cast
Zachycena komunikace po prikazu z instance demo1
> curl 169.254.169.254

![](ip-netns.jpg)